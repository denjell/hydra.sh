#!/bin/bash

###########################################################
#                                                         #
#       _/                        _/                      #
#      _/_/_/    _/    _/    _/_/_/  _/  _/_/    _/_/_/   #
#     _/    _/  _/    _/  _/    _/  _/_/      _/    _/    #
#    _/    _/  _/    _/  _/    _/  _/        _/    _/     #
#   _/    _/    _/_/_/    _/_/_/  _/          _/_/_/      #
#                  _/                                     #
#               _/_/                                      #
#                                                         #
###########################################################

#THIS IS NOT PRODUCTION READY

# must be sudo / root
# execute this file to satisfy dependency requirements
#

list:
jq
gm
feh


### need jq (critical!!!)

### FEH deps
sudo apt-get install libcurl4-openssl-dev libx11-dev libxt-dev libimlib2-dev libxinerama-dev libjpeg-progs

### FEH
git clone git://git.finalrewind.org/feh || git clone git://github.com/derf/feh.git
cd feh
make
sudo make install


#move fonts to ~/.fonts

  Permission=$(id -u)

  # Exit if not being run as Sudo
  if [ $Permission -ne 0 ]
  then
    echo -e "This build script must be run as sudo."
    exit 100
  fi
#
#

BuildError()
{
    COMMENT=$1
    COMMAND=$2
    COLUMNS=$(tput cols)
    SCRIPT=$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")
    SCRIPTDIR=$(dirname ${SCRIPT})
    TITLE="${PACKAGE} ERROR"
    echo -e "\e[1;37;41m"
    printf "%*s\n" $(($COLUMNS)) "ERROR                     "
    echo -e "\e[0m\e[31m"
    #printf "%*s\n" $(((${#TITLE}+$COLUMNS)/2)) "${TITLE}"
	echo -e "\e[36m   PACKAGE:        ${PACKAGE}"
	echo -e "   ERROR:          ${1}"
    echo -e "   DIRECTORY:      ${PWD}"
    echo -e "   SCRIPT:         ${0}" #${SCRIPTDIR}/${SCRIPT}
	echo -e "   BREAK POINT:    Line ${BASH_LINENO[$i]}"
    echo -e "\e[1;31;41m"
    printf "%*s\n" $((${COLUMNS})) ""
    echo -e "\e[0m"
	exit 101
}

BuildMessage()
{
    clear
	echo -e "\e[1;42;36m $@ \e[0m"
}

PACKAGE="apt-get"

sudo apt-get update || BuildError "Dependencies Update Failed"
sudo apt-get -y upgrade || BuildError "System Upgrade Failed"

clear
BuildMessage "Installing system dependencies"


sudo apt-get update || BuildError "Could not update repository. Is there an internet connection?"

sudo apt-get -y install autoconf automake build-essential checkinstall pkg-config git mercurial \
  chrpath git-core libperl-dev libperl5.14 bash-completion libssl-dev xdg-utils libtool detox curl \
    || BuildError "Could not install system dependencies"


cd ~/gif2mp4
git clone --depth 1 git://git.videolan.org/x264.git
cd x264
./configure --prefix="/usr/local" --enable-shared
make
sudo make install
make distclean