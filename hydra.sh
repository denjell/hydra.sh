#!/usr/bin/env bash

###########################################################
#                                                         #
#       _/                        _/                      #
#      _/_/_/    _/    _/    _/_/_/  _/  _/_/    _/_/_/   #
#     _/    _/  _/    _/  _/    _/  _/_/      _/    _/    #
#    _/    _/  _/    _/  _/    _/  _/        _/    _/     #
#   _/    _/    _/_/_/    _/_/_/  _/          _/_/_/      #
#                  _/                                     #
#               _/_/                                      #
#                                                         #
###########################################################
#  multi-headed playback and content generator for TOTO!  #
#  see LICENSE for license information.                   #
#  hydra@thereisnogame.de | There is no game              #
#  WORKS FOR ME - 33C3 - ©2016 DENJELL                    #
###########################################################

# about
# variables are all UPPERCASE
# functions are CamelCase
# which'ed external programs are underscored and all-caps: _WHICH

BEGIN=$(date +%s)
DATE=$(date)

# USE the REAL command locations because you never know which user is calling this script
command -v curl >/dev/null 2>&1 && _CURL=$(which curl) || Fail "curl not available"
command -v jq >/dev/null 2>&1 && _JQ=$(which jq) || Fail "jq not available - visit https://stedolan.github.io/jq/download/"
command -v feh >/dev/null 2>&1 && _FEH=$(which feh) || Fail "feh not available"
command -v netcat >/dev/null 2>&1 && _NETCAT=$(which netcat) || Error "netcat not available"
command -v mplayer >/dev/null 2>&1 && _MPLAYER=$(which mplayer) || Error "mplayer not available"
command -v vlc >/dev/null 2>&1 && _VLC=$(which vlc) || Error "vlc not available"
command -v aplay >/dev/null 2>&1 && _APLAY=$(which aplay) || Error "aplay not available"
command -v convert >/dev/null 2>&1 && _CONVERT=$(which convert) || Error "convert not available"
command -v gm >/dev/null 2>&1 && _GM=$(which gm) || Error "graphicsmagick not available"
command -v mimic >/dev/null 2>&1 && _MIMIC=$(which mimic) || Error "mimic not available"
command -v xdpyinfo >/dev/null 2>&1 && _XDPYINFO=$(which xdpyinfo) || Error "xdpyinfo not available"
command -v detox >/dev/null 2>&1 && _DETOX=$(which detox) || Error "detox not available"

#GET THE REAL BIN DIRECTORY IN CASE SOMETHING WEIRD IS HAPPENING
PWD=$(pwd)
SOURCE="${BASH_SOURCE[0]}"
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

# DEFINE VARIABLES
PACKAGE=$(grep 'name' "$DIR/package.json" | cut -d'"' -f4)
VERSION=$(grep 'version' "$DIR/package.json" | cut -d'"' -f4)
DESCRIPTION=$(grep 'description' "$DIR/package.json" | cut -d'"' -f4)
CONTACT=$(grep 'author' "$DIR/package.json" | cut -d'"' -f4)
URL=$(grep 'URL_FULL' "$DIR/config.json" | cut -d'"' -f4)
URL_SHORT=$(grep 'URL_SHORT' "$DIR/config.json" | cut -d'"' -f4)
API=$(grep 'API' "$DIR/config.json" | cut -d'"' -f4)
HYDRA_AGENT=$(grep 'HYDRA_AGENT' "$DIR/config.json" | cut -d'"' -f4)
KEY=$(grep 'KEY' "$DIR/config.json" | cut -d'"' -f4)
SECRET=$(grep 'SECRET' "$DIR/config.json" | cut -d'"' -f4)
STORAGE=$(grep 'STORAGE' "$DIR/config.json" | cut -d'"' -f4)
SCREENSIZE=$(grep 'SCREENSIZE' "$DIR/config.json" | cut -d'"' -f4)
WALLPAPER=$(grep 'WALLPAPER' "$DIR/config.json" | cut -d'"' -f4)

touch "${STORAGE}loglog.log"

# META FUNCTIONS
Error () # use for a non-critical service error
{
    echo "ERROR: $1"
    echo "-----------------------------------------------"
}

Fail () # use for a system relevant failure
{
    echo "FAILURE: $1"
    echo "-----------------------------------------------"
    exit 10
}

Message () # only for operator messaging
{
    echo "MESSAGE: $1"
    echo "-----------------------------------------------"
}

Log () # an event to be logged
{
    echo "$1" >> "${STORAGE}loglog.log"
}

Afterlife () # pass the pid file here to find out if it has exited
{
DEAD=false
while [[ "${DEAD}" != "true" ]] ; do
    kill -0 $(echo {$1}) 2> /dev/null || DEAD=true
    sleep 1s
done
}

Net ()
{
NET=false
while [[ "${NET}" == "false" ]]; do
    sleep 1s
    NET=$(ping -c 1 "${URL_SHORT}" | grep "time=")
    if [[ "${NET}" != "" ]]; then
        Log "${NET}"
    fi
done
}
###################################################
##                                               ##
##  APPLICATION Functions                        ##
##                                               ##
###################################################

Last () # initiate contact with TOTO
{
    RESPONSE=$(${_CURL} --user-agent "${HYDRA_AGENT}" -G --anyauth --no-progress-bar -# ${URL}${API}last -d "key=${KEY}&secret=${SECRET}" 1>&1 || Log "[$(date "+%F %T")] Curl Fail\n${RESPONSE}" )
    LASTCONTACT=$(echo ${RESPONSE} | rev | cut -d':' -f 1 | rev | cut -d'}' -f 1)
    echo "Last contact at ${LASTCONTACT}"
}

Search () # give us the details
{
    RESPONSE=$(${_CURL} --user-agent "${HYDRA_AGENT}" -G --anyauth --no-progress-bar -# ${URL}${API}search -d "key=${KEY}&secret=${SECRET}&since=${LASTCONTACT}" >&1 || Error "Curl Fail" )
    if [ $RESPONSE == '[]' ]; then
        MESSAGE="No Response at [$(date "+%F %T")]"
        Message "${MESSAGE}"
        Log "${MESSAGE}"
    else
        Message "${RESPONSE}"
        Log "${RESPONSE}"
        LASTCONTACT=$(echo ${RESPONSE} | rev | cut -d':' -f 1 | rev | cut -d'}' -f 1)
    fi
}

Ping () # use $1 here to pass a message to the server
{
    # not implemented because not necessary yet
    # but maybe nice to have
    ${_CURL} -G ${URL}${API}last -d "key=${KEY}&secret=${SECRET}"
}

Netcat () {
    # use this to telepathically communicate with other heads
    ${_NETCAT} -lk -p ${UDPPORT} | while read line
    do
        match=$(echo "$line" | grep -c 'Keep-Alive')
        if [ "$match" -eq 1 ]; then
            echo "alive"
        fi
    done
}

Detox () # clean the filename of scary characters
{
    DETOXED=$(echo '$1' | tr -d '[:alpha:]' | sed 's/[<>]//g')
}

GetMimeType () # use this to check if the file is legit before passing it on
{
    MIME=$(echo "$(file -b -k --mime-type ${ACTIVEFILE})" )
}

###################################################
##                                               ##
##  Sanity Check                                 ##
##                                               ##
###################################################

__Preroll ()
{
# setup the wallpaper
${_FEH} -ZxFYN "${WALLPAPER}" 2>&1 > /dev/null & disown
echo $! > "/tmp/pid_feh" # save its PID


# Check that a key exists. If not, use the test key and inform the user
if (( ${#KEY} == 0 )); then
    KEY=$(grep 'TESTkey' "$DIR/config.json" | cut -d'"' -f4)
    Message "USING THE DEFAULT KEY"
fi
if (( ${#SECRET} == 0 )); then
    SECRET=$(grep 'TESTsecret' "$DIR/config.json" | cut -d'"' -f4)
    Message "USING THE DEFAULT SECRET"
fi

if (( "${#SCREENSIZE}" == 0 )); then
    SCREENSIZE=$(_XDPYINFO | grep 'dimensions:'|awk '{print $2}')
fi
}

###################################################
##                                               ##
##  This is the main loop running in a subshell  ##
##                                               ##
###################################################

__Main ()
{
    for (( ;; )); do
        echo "[$(date "+%F %T")] Starting listening service."
        Net
        Last #get the last visit for the loop

        while true ; do
            RESPONSE=$(${_CURL} -A "${HYDRA_AGENT}" -G --no-progress-bar -# ${URL}${API}search -d "key=${KEY}&secret=${SECRET}&since=${LASTCONTACT}" 1>&1 || Log "Curl Fail" )
            if [ "${RESPONSE}" != '[]' ]; then
                LASTCONTACT=$(echo ${RESPONSE} | rev | cut -d':' -f 1 | rev | cut -d'}' -f 1)
                RESPONSE=$(echo ${RESPONSE} | cut -d'[' -f 2 | cut -d']' -f 1)
                JSON=$(echo ${RESPONSE} | jq '.payload' | cut -d'"' -f 2)
                if [[ ${JSON} != "" ]]; then
                    #wrap the eval in a PID check
                    Afterlife "/tmp/pid_hydraRoar"
                    eval ./hydraRoar.sh ${JSON} & disown
                    echo $! > "/tmp/pid_hydraRoar" # save its PID
                else
                    break
                fi
            else
                Message "Nothing happened at [$(date "+%F %T")]"
            fi
            # use ping instead of sleep
            # → to make sure there is a contact to server.
            Net
            #sleep 1s
        done
        Error "[$(date "+%F %T")] Listening has stopped or crashed."
    done
}

Register ()
{
    if [[ -f "$1" ]]; then
        # clean the file path before it is written to a public directory
        safeURI=$(echo "${1}" | sed "s@${dir}@${holder}@g")
        if [[ -f "${file}.register" ]]; then
            echo "${safeURI}" >> "${file}.register"
        else
            echo "${safeURI}" > "${file}.register"
        fi
    fi
    shift
}

if test $# -gt 0; then
    while test $# -gt 0; do
        case "$1" in
            -h|--help)
                echo "${PACKAGE} v${VERSION}"
                echo -e "Usage: \e[36m${PACKAGE} -h [type] -b 'body' -t [integer]\e[0m "

                echo ""
                echo 'Options:'
                echo '  -h, --head          '
                echo '                      multiple assets must be called thusly'
                echo '                      -i "[asset1.jpg],[asset2.png]"'
                echo '                      only one path may be specified, and if true, this will'
                echo '                      be the target directory used for ITO output & tmp'
                echo '  -p, --preset        use comma seperated preset values to render child assets'#
                echo '                      do not use preset and output at the same time.'
                echo '  -o, --output        create your own complex output types'
                echo '                      do not use preset and output at the same time.'
                echo '  -d, --detect        perform robust detection, report and exit.'
                echo '                      without any comma seperated sub-options it'
                echo '                      will attempt all methods of detection available'

                echo '      exiftool        read out all EXIF / XMP information'
                echo '      identify        get very detailed information about images with gm identify'
                echo '                      very specific information can be retrieved thusly:'
                echo -e "                      \e[36m${PACKAGE} -f whatever.gif -d identify=\"-format '|%m:%h:%w:%p'\"\e[0m"
                echo "                      and \"\|GIF:229:183:1\" will be returned to stdout"
                echo '      ffprobe         useful for getting itsoffset, SAR/DAR, codec etc.'
                echo -e "                      \e[36m${PACKAGE} -f whatever.nbm -d ffprobe=\"itsoffset,SAR,DAR\"\e[0m"
                echo "                      and those values will be returned"
                echo '      mime            use "xdg-mime query filetype" to return mime-type'
                echo '      lesspipe        quick way to gather infomation about a file / archive'
                echo '  -v, --version       show version of ITO & all compiled mime-classes'
                echo '  -t, --trust         this turns off antivirus and filename detox.'
                echo '  -h, --help          show this help'

                echo ""
                echo "Minimal use:"
                echo -e "  \e[36m${PACKAGE} -i \"pic.jpg\" \e[0m"
                echo "    By not supplying a mimetype, ${PACKAGE} assumes a local user, which will"
                echo "    invoke 'xdg-mime query filetype' to determine mimetype, 'clamAV' to detect"
                echo "    knowable viruses and 'detox' to clean up the filename. It will dump all"
                echo "    results in the folder where the input file is to be found, including logs."
                echo ""
                echo "Detect use:"
                echo -e "  \e[36m${PACKAGE} -i \"pic.gif\" -d mime,lesspipe,exiftool \e[0m"
                echo "    This feature will attempt to acquire any and all information about the file"
                echo "    in question by identifying its mime-type, lesspiping the file, and applying"
                echo "    exiftool, of course other options are available. Except for ffprobe, any"
                echo "    commands passed here will be used verbatim in the tool."
                echo ""
                echo "Server use:"
                echo -e "  \e[36m${PACKAGE} -i \"pic.gif\" -p \"thumb,preview,480x,1080x,mp4\" \e[0m"
                echo "    This supplies a file, a mime-type and a set of comma seperated child asset"
                echo "    results. Here we assume that the service caller has correctly identified the"
                echo "    mime-type with \"xdg-mime query filetype\", however in some cases \(such as .obj\)"
                echo "    further detection will be attempted."
                echo "    If the presets fail at any point, the process will be stopped."
                echo ""

                exit 0
                ;;
            -v|--version)
                echo "${PACKAGE} v${VERSION}"
                echo "Maintained by: ${CONTACT}"
                echo ""
                #cat "$DIR/dict/mimes.json"
                exit 0
                ;;
            *)
                echo "use --help or -h for more information"
                shift
            ;;
        esac
    done
else
    # enter the main loop
    echo -e "\e[36m${PACKAGE} v${VERSION} \e[0m\n${DESCRIPTION}"
    __Preroll
    __Main
fi