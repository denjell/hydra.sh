#!/usr/bin/env bash

###########################################################
#                                                         #
#       _/                        _/                      #
#      _/_/_/    _/    _/    _/_/_/  _/  _/_/    _/_/_/   #
#     _/    _/  _/    _/  _/    _/  _/_/      _/    _/    #
#    _/    _/  _/    _/  _/    _/  _/        _/    _/     #
#   _/    _/    _/_/_/    _/_/_/  _/          _/_/_/      #
#                  _/                                     #
#               _/_/                         SHARD        #
#                                                         #
###########################################################
#  multi-headed playback and content generator for TOTO!  #
#  see LICENSE for license information.    ©2016 DENJELL  #
###########################################################

# destroy the ramdisk if it exists
# create a new one