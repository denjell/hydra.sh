```
    _/                        _/                      
   _/_/_/    _/    _/    _/_/_/  _/  _/_/    _/_/_/   
  _/    _/  _/    _/  _/    _/  _/_/      _/    _/    
 _/    _/  _/    _/  _/    _/  _/        _/    _/     
_/    _/    _/_/_/    _/_/_/  _/          _/_/_/      
               _/                                     
          _/_/   
```

This is a multifunctional tool for TOTO Media Actuators.
It uses the 2016 actuator api from TOTO V1

See API Doc → https://flow.subsignal.org/projects/pre33/wiki/ActuatorApi

It should without problem:
- Generate .wav audio files with mimic
- Generate .mp4 video files with melt
- Generate single-frame .mp4 as a video typed image (to avoid framebuffer problems)
- Playback audio, video and image files from local or from a network
 
Structure for the action is e.g.:

HEAD: text/voice/uri | PAYLOAD: body

HEAD
text: write a text in a font described in body to an image, turn this image into a video, play loop
voice: if the "detox'd" file doesn't exist, create MIMIC voice → save it and play it back
uri: if the "detox'd" file doesn't exist locally get the resource from an uri, detox it, check its MEDIA type, save it and play ASAP according to detected MEDIA type

PAYLOAD:
body: use the text to generate an audio or a pic or to deliver the uri for an existing media file


It is composed for Ubuntu 16.04 and should be able to run in a POSIX environment. Its dependencies are:
 
- MIMIC (from Git)
- MELT Framework
- MPLAYER (where available) or FFPLAY (on arm[rPi])
- GRAPHICSMAGICK
- WGET
- MAKE

For testing we will use these credentials

`name:hydra_test`
`key:01be3e7f-9467-4f65-9653-a2de907b9a31`
`secret:LOUSd4FMJLBUwlNesir5`

This is a valid request:
```$ curl https://ting.trace.io/api/1/session/act/last -G -d 'key=01be3e7f-9467-4f65-9653-a2de907b9a31&secret=LOUSd4FMJLBUwlNesir5'``` 

and returns this:
`{"name":"hydra_1","payload":"voice:there is no game","ts":1481129084829}`

HEAD: text/voice/uri/local/alert/drama | PAYLOAD: body

HEAD
text: write a text into an image, turn this image into a video, play loop
voice: if the "detox'd" file doesn't exist, create MIMIC voice → save it and play it back
uri: if the "detox'd" file doesn't exist locally get the resource from an uri, detox it, check its MEDIA type, save it and play ASAP according to detected MEDIA type
local: playback a local file on the machine running hydra.sh
it_alert: text alert (with sound) for response error of a device
dramanet: text alert for a stage direction in the reisebüro

PAYLOAD:
body: use the text to generate an audio or a pic or to deliver the uri for an existing media file
if you use a space or illegal character in the payload for uri, then it *might* fail.

Valid text-type payload:
--head "text" --payload "There is no game."
// hydra.sh will create an image if thereisnogame.jpg does not exist and then show it with a fullscreen image viewer

Valid voice-type payload
--head "voice" --payload "There is no game."
// hydra.sh will create a soundfile if thereisnogame.wav does not exist and then play it back immediately

Valid uri-type payload
--head uri --payload "10.10.0.45/content/video.mp4"
// hydra will attempt to download the file and then play it back appropriately. Use this only in emergencies because the first time it is downloaded there will be lag. Prefer to host all media content locally.

Valid local-type payload: 
--head local --payload "glitchy-logo_static-background_45sec_720p.mp4"
 - is the same as -
-h "local" -p "glitchy-logo_static-background_45sec_720p.mp4"
// hydra.sh will check the config file's setting for file storage and look there and play back the file if found. otherwise continue what it was doing.

Valid it_alert-type payload
--head it_alert --payload "keyhole 12:no ping"

Valid dramanet-type payload
--head dramanet --payload "haladriel with 20 golden hula-hoops to brunnen"

There are four main calls in the 33C3 version of the API:
/last 
/search
/ping (only for contacting the server to pass a message such as status

INSTALL
- git clone this repo / unpack the latest tarball
- if you are making an audio head, you will at least need aplay, which should be available on most linux distros.
- if you are on an arm device, you will probably have to declare which audio device to use, which can be tricky.

```
nohup ${mplayer} -noconfig all -nolirc -slave -ao alsa:device=hw=1 -vo null -idle -input file=${fifo} 2>/dev/null & disown -h
```

```
http://blog.dustinkirkland.com/2011/02/introducing-run-one-and-run-this-one.html
```
broadcast
- ISNESSY receives a message
 - she broadcasts a hydraRoar.sh command line via netcat to all of those listening.
 - one for each head → distributes the message over udp to the others?



- bestiary (encrypted)
 - if de-encryption key comes 