
###########################################################
#                                                         #
#       _/                        _/                      #
#      _/_/_/    _/    _/    _/_/_/  _/  _/_/    _/_/_/   #
#     _/    _/  _/    _/  _/    _/  _/_/      _/    _/    #
#    _/    _/  _/    _/  _/    _/  _/        _/    _/     #
#   _/    _/    _/_/_/    _/_/_/  _/          _/_/_/      #
#                  _/                                     #
#               _/_/                         ROAR         #
#                                                         #
###########################################################
#  multi-headed playback and content generator for TOTO!  #
#  see LICENSE for license information.    ©2016 DENJELL  #
###########################################################

# variables are all UPPERCASE
# functions are CamelCase
# hydraParse.sh calls individual playback heads
# is called from hydra.sh in a subshell

# Testing hooks / overrides
# echo $# $*
_FEH=$(which feh)
_MIMIC=$(which mimic)
_MPLAYER=$(which mplayer)
_APLAY=$(which aplay)
_CURL=$(which curl)

if test ${STORAGE}; then echo "using ${STORAGE}"; else STORAGE="/media/hydra/"; fi

################### METAS ######################

Getfiledetails () {
    BASE=$(basename ${1})
    FILESTUB="${BASE%.*}"
    SUFFIX="${1##*.}"
}

Exists () {
    # see if the file exists, if so DON'T create it
    EXISTS=false
    FILENAME=false
    LOWERCASE=${1,,}
    FILENAME=$(echo "${LOWERCASE}" | cut -c1-16 | tr -cd '[:alnum:]')
    if test -f "${STORAGE}${FILENAME}.${SUFFIX}"; then EXISTS=true; fi
}

Theylive ()     # $1=PID
{               # pass the pid file here to find out if it has exited
ALIVE=false
kill -0 $(echo ${1}) 2> /dev/null && ALIVE=true
}

Afterlife ()    # $1=PID
{               # pass the pid file here to find out if it has exited
DEAD=false
while [[ "${DEAD}" != "true" ]] ; do
    kill -0 $(echo ${1}) 2> /dev/null || DEAD=true
    sleep 1s # the sleep after death
done
rm ${1}
}

Charon ()       # $1=PID $2=TTL
{               # pass the pid file and
sleep ${2}s || sleep 33s
kill $(echo ${1})
rm ${1}
}

Loading ()
{
    echo Loading
    #mplayer -fs "./storage/loading.mp4" -loop 0 & disown
    #echo $! > "/tmp/pid_feh" # register PID_feh from mplayer... (hack your own code)
}
################### HEADS ######################

Text ()
{
    SUFFIX="png"
    HEAD="TEXT-"
    FONT="./fonts/NotoMono-Regular.ttf"
    TEXT=${1}
    Exists "$1"
    if [[ "${EXISTS}" == true ]]; then
        echo Eye
        Eye || echo "Error: Eye"
    else
        echo Magick Eye
        Magick && Eye || echo "Error: Magick Eye"
    fi
}
Linearc () {
    SUFFIX="png"
    HEAD="LINEARC-"
    FONT="./fonts/Linear_C_2.0.3.ttf"
    TEXT=${1,,} # make lowercase because font. meh :/
    Exists "$1"
    if [[ "${EXISTS}" == true ]]; then
        echo Eye
        Eye
    else
        echo Magick Eye
        Magick && Eye || echo "Error: Magick Eye"
    fi
}
Voice () {
    SUFFIX="wav"
    HEAD="VOICE-"
    Exists "$1"
    if [[ "${EXISTS}" == true ]]; then
        Aplay
    else
        Mimic
        Aplay
    fi
}
Uripic () {
    HEAD="URIPIC-"
    Getfiledetails "${BODY}"
    Exists "${FILESTUB}"
    if [[ "${EXISTS}" == "true" ]]; then
        Eye
    else
        Curluri
        Eye
     fi
}
Uriprint () {
    HEAD="URIPRINT-"
    Getfiledetails "${BODY}"
    Exists "${FILESTUB}"
    if [[ "${EXISTS}" == "true" ]]; then
        Print
    else
        Curluri
        Print
     fi
}
Urisnd () {
    mplayer "${BODY}" 2>&1 > /dev/null & disown
    echo $! > "/tmp/pid_feh" # register PID from feh
}
Urivid () {
    mplayer "${BODY}" 2>&1 > /dev/null & disown
    echo $! > "/tmp/pid_feh" # register PID from feh
}
IT_Alert () {
    #send to scrolling listener
    echo "${1}"
}
Dramanet () {
    #send to scrolling listener
    echo "${body}" >> ./dramanet.txt
    Theylive '/tmp/dramanet.pid'
    if [[ ${ALIVE} != "true" ]]; then
        tail -f /tmp/dramanet.txt
        echo $! > "/tmp/dramanet.pid" # register PID from feh
    fi
}
Telepathy () {
    #broadcast to all heads
    echo "${1}"
}
################ EXECUTIVES #################
Curluri ()
{
    ${_CURL} -o "${STORAGE}${HEAD}${FILENAME}.${SUFFIX}" "${BODY}"
}

Mplayer ()          # playback video / audio
{
    ${_MPLAYER} -nolirc -fs "${STORAGE}${HEAD}${FILENAME}.${SUFFIX}" -loop ${TIME} 2>&1 > /dev/null & disown
}

Aplay ()            # playback audio
{
    $_APLAY "${STORAGE}${HEAD}${FILENAME}.${SUFFIX}" || Error "Aplay ${STORAGE}${HEAD}${FILENAME}.${SUFFIX}"
}

Magick ()           # create image
{
        convert  -background '#0004' -gravity center -size 1280x720 -font "${FONT}" -fill dodgerblue -stroke white -strokewidth 3 caption:"${TEXT}" ${STORAGE}${HEAD}${FILENAME}.${SUFFIX}

        #gm convert -background '#000' -size 1280x720 -gravity center -pointsize 50 -fill dodgerblue -stroke '#ffe' -draw "text 0,0 '${TEXT}'" "./storage/background_black.png" "${STORAGE}${HEAD}${FILENAME}.${SUFFIX}"

        #gm convert -size 1280x720 -gravity center -pointsize 50 -fill dodgerblue -stroke '#ffe' -draw "text 0,0 '${BODY}'" "./storage/background_black.png" "${STORAGE}${HEAD}${FILENAME}.${SUFFIX}"
        #gm convert  -background '#0004' -gravity center -size 1280x720 -font "${FONT}" -pointsize 50 -fill dodgerblue -stroke '#ffe' -strokewidth 3 caption:"${TEXT}" ${STORAGE}${HEAD}${FILENAME}.${SUFFIX}

}

Eye () { # display image
    # if feh is running, kill it.
    kill $(cat /tmp/pid_feh) 2> /dev/null

    # start a new feh
    ${_FEH} -ZxFYN "${STORAGE}${HEAD}${FILENAME}.${SUFFIX}" 2>&1 > /dev/null & disown
    echo $! > "/tmp/pid_feh" # register PID from feh

    # everything will pass
    Charon $(cat "/tmp/pid_feh") ${TIME}

    # historical versions for later use, maybe. :/
    #gm display "${STORAGE}live_image.${SUFFIX}" -update 1 +progress 2>&1 > /dev/null & disown -h # → can't get to full screen
    #eom "${STORAGE}${FILENAME}.${SUFFIX}" -f -c 2>&1 > /dev/null & disown # (only in ubuntu mate)
    #_DISPLAY -geometry $SCREENSIZE -immutable ./storage/tmp.png # same as gm
}

Print () { # print text
    # start a new feh
    ./libs/print.py "${STORAGE}${HEAD}${FILENAME}.${SUFFIX}" 2>&1 > /dev/null & disown
}

Mimic () {
    # wait for mimic to finish (because we don't want tons of these spawning)
    Afterlife "/tmp/pid_mimic"
    $_MIMIC --setf duration_stretch=0.75 --setf int_f0_target_mean=42 --sets join_type=simple_join -voice "./voices/ting.flitevox" -t "${BODY}" -o "${STORAGE}${HEAD}${FILENAME}.${SUFFIX}" #2>&1 > /dev/null
    echo $! > "/tmp/pid_mimic" # save its PID
}

Scroller () {
    #append logfile
    # run-mailcap as display?
    echo "${1}"
}

if test $# -gt 0; then
    while test $# -gt 0; do
        case "$1" in
            -h|--head)
                shift
                if test $# -gt 0; then
                    next=$(printf "%.1s" $1)
                    if test $next != "-"; then
                        case "${1}" in
                            "text")
                                HEAD="Text"
                                shift
                            ;;
                            "linearc")
                                HEAD="Linearc"
                                shift
                            ;;
                            "voice"|"speech")
                                HEAD="Voice"
                                shift
                            ;;
                            "uripic")
                                HEAD="Uripic"
                                shift
                            ;;
                            "urisnd")
                                HEAD="Urisnd"
                                shift
                            ;;
                            "urivid")
                                HEAD="Urivid"
                                shift
                            ;;
                            "local")
                                HEAD="Local"
                                shift
                            ;;
                            "it_alert")
                                HEAD="IT_alert"
                                shift
                            ;;
                            "dramanet")
                                HEAD="Dramanet"
                                shift
                            ;;
                            "uriprint")
                                HEAD="Uriprint"
                                shift
                            ;;
                        esac
                    fi
                else
                    Error "No Head Specified."
                    exit
                fi

                ;;
            -b|--body)
                shift
                    if test $# -gt 0; then
                        next=$(printf "%.1s" $1)
                        if test $next != "-"; then
                            BODY="${1}"
                        fi
                    fi
                    shift
                ;;
            -t|--time)
                shift
                    if test $# -gt 0; then
                        next=$(printf "%.1s" $1)
                        if test $next != "-"; then
                            TIME="${1}"
                        fi
                    fi
                    shift
                ;;
              -s|--speed)
                shift
                    if test $# -gt 0; then
                        next=$(printf "%.1s" $1)
                        if test $next != "-"; then
                            SPEED="${1}"
                        fi
                    fi
                    shift
                ;;
            esac
        done
else
    echo "No Head Specified."
    exit 0
fi
echo ${HEAD} \'"${BODY}"\'
eval ${HEAD} \'"${BODY}"\' || echo "error"
