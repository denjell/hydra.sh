#!/usr/bin/env python3

import time
import math
import sys
from subprocess import Popen, PIPE, STDOUT

from_header    = 'OUTER SPACE'
to_header      = 'TRAVELER'
subject_header = 'LOGBOOK #42'

max_lines = 36
max_text_width = 30
max_text_lines = 19

now = int(math.floor(time.time()))

if len(sys.argv) != 2:
    print('usage: %s <text file>' % sys.argv[0])
    sys.exit(1)

try:
    with open(sys.argv[1]) as f:
        input_text = f.read().replace('\n', ' ')
except Exception as e:
    print('Error reading file %s: %s' % (sys.argv[1], e))
    sys.exit(2)

def line(text):
    l = '  *  ' 
    l += text
    l += ' ' * (35 - len(l))
    l += '  * '
    return l

def text(txt):
    words = txt.split()

    lines = []
    current_line = ''

    while words:
        word = words.pop(0)
        if len(lines) >= max_text_lines:
            lines.pop()
            lines.append(line('[=== TRANSMISSION ABORTED ===]'))
            break

        if len(current_line + ' ' + word) > max_text_width + 1:
            if not current_line:
                lines.append(line(word[:max_text_width]))
                words.insert(0, word[max_text_width:])
            else:
                lines.append(line(current_line.strip()))
                current_line = ''
                words.insert(0, word)
        else:
            current_line += ' ' + word

    if len(current_line) > 0:
        lines.append(line(current_line.strip()))

    while len(lines) < max_text_lines:
        lines.append(line(''))

    return '\n'.join(lines) 

def do_print(msg):
    print(msg)

    p = Popen(['lp', '-d', 'labelpr'], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    print(p.communicate(input = str.encode(msg))[0])

lines = []

lines.append('                                       ')
lines.append('  $ cat /dev/random                    ')
lines.append('                                       ')
lines.append('  ************************************ ')
lines.append(line(''))
lines.append(line('FROM:    %s' % from_header))
lines.append(line('TO:      %s' % to_header))
lines.append(line('SUBJECT: %s' % subject_header))
lines.append(line(''))
lines.append(line('Stardate %s' % now))
lines.append(line(''))
lines.append(text(input_text))
lines.append(line(''))
lines.append(line('--'))
lines.append(line('PS: THERE IS NO GAME'))
lines.append(line(''))
lines.append('  ************************************ ')
lines.append('                                       ')

do_print('\n'.join(lines))
